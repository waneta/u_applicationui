﻿using UnityEngine;
using System.Collections;
using Client.ApplicationUI;
using Client.Utils;

public class Example_1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LogManager.Instance.setLogLevel (LogManager.LOGLEVEL.INFO);
	}

	void OnGUI(){
		if (GUILayout.Button (" UI init")) {
			ApplicationUI.Instance.init (UIResType.Example);
		}

		if (GUILayout.Button ("Open Main UI")) {
			ApplicationUI.Instance.openWindowObject ("MainUI", SHOW_EFFECT.FADE);
		}

		if (GUILayout.Button ("open Countdown")) {
			ApplicationUI.Instance.openWindowObject ("Countdown", SHOW_EFFECT.ZOOM,true,true);
		}

		if (GUILayout.Button ("close Countdown")) {
			ApplicationUI.Instance.closeWindowObject ("Countdown", SHOW_EFFECT.ZOOM,true);
		}


		if (GUILayout.Button ("open MessageBox1Button")) {
			ApplicationUI.Instance.openWindowObject ("MessageBox1Button", SHOW_EFFECT.ZOOM);
		}

		if (GUILayout.Button ("close MessageBox1Button")) {
			ApplicationUI.Instance.closeWindowObject ("MessageBox1Button", SHOW_EFFECT.ZOOM,true);
		}



		if (GUILayout.Button ("MessageBox2Button")) {
			ApplicationUI.Instance.openWindowObject ("MessageBox2Button", SHOW_EFFECT.FADE);
		}

		if (GUILayout.Button ("MessageBoxCheckBox")) {
			ApplicationUI.Instance.openWindowObject ("MessageBoxCheckBox", SHOW_EFFECT.FADE);
		}


	}
	// Update is called once per frame
	void Update () {
	
	}
}
