﻿using UnityEngine;
using System.Collections;
using Client.ApplicationUI;

namespace UI.Example{
	public class MainUI : MonoBehaviour {


		public RectTransform anchor_Top;

		private TopMenu _topMenu;
		public TopMenu topMenu{
			get{ 
				if (_topMenu == null) {
					_topMenu = ApplicationUI.Instance.LoadUI<TopMenu> ("TopMenu", anchor_Top);
				}
				return _topMenu;
			}
		}
		// Use this for initialization
		void Start () {
			anchor_Top = transform.FindChild ("Anchor_Top").GetComponent<RectTransform>();
			TopMenu _u = topMenu;
			Debug.Log ("Load TopMenu"+_u.gameObject.name);
		}
		
		// Update is called once per frame
		void Update () {
		
		}

	}
}
