﻿using UnityEngine;
using System.Collections;

namespace Client.ApplicationUI
{ 
    public class PromptPreConfig  {
		const string prefsuff = "CheckBox";
        public static void addOperate(byte mType, int promptId, bool isChoose){
            if(mType == 1){
				PlayerPrefs.SetInt(prefsuff + promptId,isChoose?1:0);
            }
        }

        public static bool getOperate(byte type,int promptId){
            if(type == 1){
				int value = PlayerPrefs.GetInt(prefsuff + promptId);
                if(value > 0){
                    return true;
                }else{
                    return false;
                }
            }
            return false;
        }

    }
}
