﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Client.Language;
using Client.Utils;

namespace Client.ApplicationUI
{
	public class PromptObject { 
		private Int32 m_nMessageID=0;				  //// 消息Id 
		public Int32 ID { get { return m_nMessageID; } } 
		public Int32 MessageID { get { return m_nMessageID; } } 
		private Int32 m_nMessageInfo=0;				  //// 消息内容 
		public Int32 MessageInfo { get { return m_nMessageInfo; } } 
		private bool m_bPrompt=false;				  //// 是否在提示栏显示 
		public bool Prompt { get { return m_bPrompt; } } 
		private bool m_bNotice=false;				  //// 是否在公告栏显示 
		public bool Notice { get { return m_bNotice; } } 
		private bool m_bNetPrompt=false;				  //// 是否在连接界面显示 
		public bool NetPrompt { get { return m_bNetPrompt; } } 
		private Int32 m_nMessageBox=0;				  //// 弹出的MESSAGEBOX类型 
		public Int32 MessageBox { get { return m_nMessageBox; } } 
		private Int32 m_nShowTime=0;				  //// BOX显示时间 
		public Int32 ShowTime { get { return m_nShowTime; } } 
		private Int32 m_nButton1Text=0;				  //// 按钮1文字（MESSAGEBOX的按钮文字） 
		public Int32 Button1Text { get { return m_nButton1Text; } } 
		private Int32 m_nButton2Text=0;				  //// 按钮2文字（MESSAGEBOX的按钮文字） 
		public Int32 Button2Text { get { return m_nButton2Text; } } 
		private Int32 m_nButtonCheckText=0;				  //// 不再提示文字 
		public Int32 ButtonCheckText { get { return m_nButtonCheckText; } } 
		public PromptObject(){
			
		}
	}
    public class PromptManager : Singleton<PromptManager>
    {
        public delegate IEnumerator OnMessageBoxButtonClick(object args);
        public delegate IEnumerator OnChooseCallBack(int itemId, object args);
        //public delegate IEnumerator OnTimeCallBack(object args);

		Dictionary<Int32,PromptObject> promptList = new Dictionary<int, PromptObject> ();
		void AddPrompt(PromptObject o){
			if (!promptList.ContainsKey (o.ID)) {
				promptList.Add (o.ID, o);
			}
		}
		PromptObject GetPrompt(int id){
			if (promptList.ContainsKey (id)) {
				return promptList [id];
			}
			return null;
		}

		public override void initialize(){
			AddPrompt (new PromptObject ());

		}


        public void ShowPrompt(string format, params object[] args)
        {
            ShowNormalPrompt(format, args);
        }
        /// <summary>
        /// isNeedCloseBg true 功能：点击弹窗外边框以外的背景，相当于点击取消键 //此函数尽在Report功能
        /// </summary>
        /// <param name="promptId"></param>
        /// <param name="clickOk"></param>
        /// <param name="clickCancel"></param>
        /// <param name="callBackArgs"></param>
        /// <param name="isNeedCloseBg"></param>
        /// <param name="args"></param>  
        public void ShowPrompt(int promptId, OnMessageBoxButtonClick clickOk, OnMessageBoxButtonClick clickCancel, object callBackArgs,bool isNeedCloseBg)
        {
			PromptObject prompt = GetPrompt(promptId);
            if (prompt == null)
            {
                LogManager.GetInstance().error("prompt == null   {0}", promptId);
                return;
            }
            string format = LanguageCommon.getStringMessage(prompt.MessageInfo);
            if (string.IsNullOrEmpty(format))
            {
                LogManager.GetInstance().error("Prompt format is null  promptId = {0}   StringMessageId = {1}", promptId, prompt.MessageInfo);
                return;
            }
           
			float length = prompt.ShowTime< 0 ? -1 : prompt.ShowTime;      
            if (prompt.MessageBox == 2)
            {
				MessageBox2Button messageBox = ApplicationUI.Instance.openWindowObject<MessageBox2Button>("MessageBox2Button", SHOW_EFFECT.ZOOM, true, isNeedCloseBg);
                messageBox.initialize(length, clickOk, clickCancel, callBackArgs, LanguageCommon.getStringMessage(prompt.Button1Text), LanguageCommon.getStringMessage(prompt.Button2Text),format);
            }
           
        }
        public void ShowPrompt(int promptId, OnMessageBoxButtonClick clickOk, OnMessageBoxButtonClick clickCancel, object callBackArgs, params object[] args)
        {
			PromptObject prompt = GetPrompt(promptId);
            if (prompt == null)
            {
                LogManager.GetInstance().error("prompt == null   {0}", promptId);
                return;
            }
            string format = LanguageCommon.getStringMessage(prompt.MessageInfo);
            if (string.IsNullOrEmpty(format))
            {
                LogManager.GetInstance().error("Prompt format is null  promptId = {0}   StringMessageId = {1}", promptId, prompt.MessageInfo);
                return;
            }
            //if (prompt.Prompt == true)
            //    ShowNormalPrompt(format, args);
            //if (prompt.Notice == true)
            //	ShowNotice(format,args);
            //if (prompt.NetPrompt == true)
            //	ShowNetworkPrompt(format,args);
			float length = prompt.ShowTime<0 ? -1 : prompt.ShowTime;
            if (prompt.MessageBox == 1)
            {
                if(args.Length>0){
					MessageBox1Button messageBox = ApplicationUI.Instance.openWindowObject<MessageBox1Button>("MessageBox1Button", SHOW_EFFECT.ZOOM,true,false);
                    messageBox.initialize(length, clickOk, clickCancel, callBackArgs, LanguageCommon.getStringMessage(prompt.Button1Text), format, args);
                }
                else{
					MessageBox1Button messageBox = ApplicationUI.Instance.openWindowObject<MessageBox1Button>("MessageBox1Button", SHOW_EFFECT.ZOOM);
                    messageBox.initialize(length, clickOk, clickCancel, callBackArgs, LanguageCommon.getStringMessage(prompt.Button1Text), format, args);

                }
              
            }
            else if (prompt.MessageBox == 2)
            {
				MessageBox2Button messageBox = ApplicationUI.Instance.openWindowObject<MessageBox2Button>("MessageBox2Button", SHOW_EFFECT.ZOOM,true);
                messageBox.initialize(length, clickOk, clickCancel, callBackArgs, LanguageCommon.getStringMessage(prompt.Button1Text), LanguageCommon.getStringMessage(prompt.Button2Text), format, args);
            }
            else if (prompt.MessageBox == 3)
            {
				MessageBoxCheckBox messageBox = ApplicationUI.Instance.openWindowObject<MessageBoxCheckBox>("MessageBoxCheckBox", SHOW_EFFECT.ZOOM,true);
                messageBox.initialize(1, promptId, clickOk, clickCancel, callBackArgs,
                    LanguageCommon.getStringMessage(prompt.Button1Text),
                    LanguageCommon.getStringMessage(prompt.Button2Text),
                    LanguageCommon.getStringMessage(prompt.ButtonCheckText),
                    format, args);
            }
        }
        public void ShowPrompt(int promptId, params object[] args)
        {
            ShowPrompt(promptId, null, null, null, args);
        }

		public void ShowCountDown(int length, CountdownUI.OnCallBack callBack, object callBackArgs)
        {
			CountdownUI countDown = ApplicationUI.Instance.openWindowObject<CountdownUI>("CountDownUI",SHOW_EFFECT.NONE);
			countDown.StartCountDown(length, callBack,callBackArgs);
        }
        
        public void ShowOnlyString(int mode, string message, params object[] args)
        {
            if (mode == 0)
                ShowNormalPrompt(message, args);
            if (mode == 1)
                ShowNotice(message, args);
        }
		public void ShowNormalPrompt(string messsage ,params object[] args){
			
		}

		public void ShowNotice(string messsage ,params object[] args){

		}

    }
}

