﻿using UnityEngine;
using UnityEngine.UI;
using Client.Utils;
namespace Client.ApplicationUI
{
    public class MessageBoxCheckBox : MonoBehaviour
    {
        private object _callBackArgs;
        private PromptManager.OnMessageBoxButtonClick _clickOK;
        private PromptManager.OnMessageBoxButtonClick _clickCancel;
        public Text text;
        public Text buttonOkText;
        public Text buttonCancelText;
        public Toggle checkBox;
        public Text checkBoxText;
        private byte mType = 0;
        private int mOperate = -1;
        private bool bCallBack = false;
        public bool isoverSorting = false;

		void Start(){
			text = transform.FindChild ("Plane/Anchor_Center/context").GetComponent<Text>();


			buttonOkText = transform.FindChild ("Plane/Anchor_Button/Button1/Text").GetComponent<Text>();
			buttonCancelText = transform.FindChild ("Plane/Anchor_Button/Button1/Text").GetComponent<Text>();
			checkBox = transform.FindChild ("Plane/Anchor_Center/Toggle").GetComponent<Toggle>();

			checkBoxText = transform.FindChild ("Plane/Anchor_Center/Toggle/Label").GetComponent<Text>();

		}
        public void initialize(byte type, int promptId, PromptManager.OnMessageBoxButtonClick clickOk, PromptManager.OnMessageBoxButtonClick clickCancel, object callBackArgs, string okText, string cancelText, string checkText, string format, params object[] args)
        {
            bCallBack = false;
            mType = type;
            mOperate = promptId;


            if (this.gameObject.GetComponent<Canvas>() != null)
            {
                this.gameObject.GetComponent<Canvas>().overrideSorting = isoverSorting;
            }
            /*if (PlayerConfig.getOperate(type, promptId))
            {
                if (clickOk != null) Common.StartCoroutine(clickOk(callBackArgs));
                ApplicationUI.Instance.closeWindowObject(WINDOWOBJECT_TYPE.MESSAGEBOXCHECKBOX,SHOW_EFFECT.NONE,true);
                return;
            }*/
            text.text = string.Format(format, args);
            if (!string.IsNullOrEmpty(okText)) buttonOkText.text = okText;
            if (!string.IsNullOrEmpty(cancelText)) buttonCancelText.text = cancelText;
            if (!string.IsNullOrEmpty(checkText)) checkBoxText.text = checkText;
			checkBox.isOn = PromptPreConfig.getOperate(type, promptId);
            _clickOK = clickOk;
            _clickCancel = clickCancel;
            _callBackArgs = callBackArgs;
        }
        public void OnClickOK()
        {
            if (bCallBack == true)
                return;
            bCallBack = true;
			if (/*checkBox.isOn && */mType != 0 && mOperate != -1) PromptPreConfig.addOperate(mType, mOperate, checkBox.isOn);
            if (_clickOK != null) Common.StartCoroutine(_clickOK(_callBackArgs));
			ApplicationUI.Instance.closeWindowObject("MessageBoxCheckBox",SHOW_EFFECT.NONE,true);
        }
        public void OnClickCancel()
        {
            if (bCallBack == true)
                return;
            bCallBack = true;
            if (_clickCancel != null) Common.StartCoroutine(_clickCancel(_callBackArgs));
			ApplicationUI.Instance.closeWindowObject("MessageBoxCheckBox", SHOW_EFFECT.NONE, true);
        }
    }
}
