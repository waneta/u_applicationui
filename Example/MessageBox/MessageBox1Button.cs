﻿using UnityEngine;
using UnityEngine.UI;
using Client.Utils;
namespace Client.ApplicationUI {
	
    public class MessageBox1Button : MonoBehaviour {
        private enum MESSAGEBOX_TYPE
        {
            NORMAL,
            TIME,
        }
        private object _callBackArgs;
        private PromptManager.OnMessageBoxButtonClick _clickOK;
        private PromptManager.OnMessageBoxButtonClick _clickCancel;
        private MESSAGEBOX_TYPE _type;
        private float _endTime;
        public Text text;
        public Text times;
        public Text buttonOkText;

        private bool bCallBack = false;

		void Start(){
			text = transform.FindChild ("Plane/Anchor_Center/context").GetComponent<Text>();
			times = transform.FindChild ("Plane/Anchor_Center/time").GetComponent<Text>();

			buttonOkText = transform.FindChild ("Plane/Anchor_Button/Button/Text").GetComponent<Text>();

		}

        // Update is called once per frame
        void FixedUpdate() {
            if (_type == MESSAGEBOX_TYPE.TIME)
            {
                int lastTime = (int)(_endTime - Time.time);
                if (lastTime < 0)
                    lastTime = 0;
                times.text = lastTime.ToString();
                if (lastTime <= 0)
                    OnClickCancel();
            }
        }
        public void initialize(float lengthTime, PromptManager.OnMessageBoxButtonClick clickOk, PromptManager.OnMessageBoxButtonClick clickCancel, object callBackArgs, string buttonText, string format, params object[] args)
        {
            bCallBack = false;
            text.text = string.Format(format, args);
            if (!string.IsNullOrEmpty(buttonText))
                buttonOkText.text = buttonText;
            if (lengthTime <= 0)
            {
                _type = MESSAGEBOX_TYPE.NORMAL;
				times.gameObject.SetActive(false);
            }
            else
            {
                _type = MESSAGEBOX_TYPE.TIME;
                _endTime = Time.time + lengthTime;
				times.gameObject.SetActive(true);
            }
            _clickOK = clickOk;
            _clickCancel = clickCancel;
            _callBackArgs = callBackArgs;
           
        }
        public void OnClickOK()
        {
            if (bCallBack == true)
                return;
            bCallBack = true;
            if (_clickOK != null) Common.StartCoroutine(_clickOK(_callBackArgs));
            ApplicationUI.Instance.closeWindowObject("MessageBox1Button", SHOW_EFFECT.NONE, true);
        }
        public void OnClickCancel()
        {
            if (bCallBack == true)
                return;
            bCallBack = true;
            if (_clickCancel != null) Common.StartCoroutine(_clickCancel(_callBackArgs));
			ApplicationUI.Instance.closeWindowObject("MessageBox1Button", SHOW_EFFECT.NONE, true);
        }
    }
}
