﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using Client.Utils;
namespace Client.ApplicationUI
{
	public class CountdownUI : IWindow
    {

        public const int MIN_COUNT_DOWN_SECONDS = 3;
        public int count = MIN_COUNT_DOWN_SECONDS;
        public Image[] numberSprite;
        public Image numberBg;

        //public TweenScale _tweenScale;

		public delegate void OnCallBack(object args);
        public OnCallBack OnCallBackMethod;
		public object callBackArgs;
        private int rotationValue = 0;
        private int rateCircle = 3;
        //是否继续倒计时，用于临时终止；
        public bool flagCountDown;

		Coroutine  _coroutine ;
        public void Start()
        {

			numberSprite = new Image[5];
			numberSprite [0] = transform.FindChild ("Plane/Image_1").GetComponent<Image>();
			numberSprite [1] = transform.FindChild ("Plane/Image_2").GetComponent<Image>();
			numberSprite [2] = transform.FindChild ("Plane/Image_3").GetComponent<Image>();
			numberSprite [3] = transform.FindChild ("Plane/Image_4").GetComponent<Image>();
			numberSprite [4] = transform.FindChild ("Plane/Image_5").GetComponent<Image>();

			StartCountDown ();
        }
        public void StartCountDown()
        {
            StartCountDown(MIN_COUNT_DOWN_SECONDS);
        }
        public void StartCountDown(int time)
        {
			StartCountDown(time, null,null);
        }

		public void StartCountDown(int time, OnCallBack _method,object _callBackArgs)
        {
            flagCountDown = true;
            numberBg.gameObject.SetActive(true);

            count = time;
            OnCallBackMethod = _method;
			callBackArgs = _callBackArgs;
            if (count < MIN_COUNT_DOWN_SECONDS)
            {
                count = MIN_COUNT_DOWN_SECONDS;
            }

            StartCoroutine("CountDwon_impl");

        }
        void Update()
        {
            if (flagCountDown)
            {
                rotationValue += rateCircle;
                numberBg.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -rotationValue));
                if (rotationValue > 360)
                    rotationValue = 0;
            }
        }
        public void StopCountDown()
        {

            //修改倒计时标记
            flagCountDown = false;

            //清空倒计时回调；
            OnCallBackMethod = null;
            numberBg.gameObject.SetActive(false);
            //numberSprite.gameObject.SetActive(false);
            //停止协程
            StopCoroutine("CountDwon_impl");

            count = 0;
        }


        IEnumerator CountDwon_impl()
        {

            for (int i = count; i >= 0 && flagCountDown; i--)
            {
                if (i != count)
                {
                    //如果继续倒计时，则等待1s
                    yield return new WaitForSeconds(1f);
                }
                if (i > 0)
                {
                    //numberSprite.spriteName = "number_" + i.ToString();
                    int index = count < 0 ? 0 : i - 1;
                    SetNumberImageVis(index);
                    SoundManager.Instance.playSound(SOUND.COLLECT, gameObject);
                }


            }
            numberBg.gameObject.SetActive(false);

            //倒计时结束，回调方法
            if (flagCountDown && OnCallBackMethod != null)
            {
				OnCallBackMethod(callBackArgs);
            }

			//this.OnClickCloseWindow ();
            yield break;
        }
        void SetNumberImageVis(int index)
        {
            for(int i = 0; i< numberSprite.Length; i++)
            {
                if(i == index)
                {
                    numberSprite[i].gameObject.SetActive(true);
                }
                else
                {
                    numberSprite[i].gameObject.SetActive(false);
                }
            }
        }
        public void OnFinshTween()
        {
            //_tweenScale.duration = 0.01f;
            //_tweenScale.PlayReverse();
        }
    }
}