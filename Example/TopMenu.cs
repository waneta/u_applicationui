﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace UI.Example{
	public class TopMenu : UIBehaviour {

		public Text titleLabel;
		protected override void Start(){
			titleLabel = transform.FindChild ("Text").GetComponent<Text>();
		}
		protected override void OnBeforeTransformParentChanged ()
		{
			base.OnBeforeTransformParentChanged ();
			//Debug.LogError ("OnBeforeTransformParentChanged");
		}
		protected override void OnTransformParentChanged ()
		{
			base.OnTransformParentChanged ();
			//Debug.LogError ("OnTransformParentChanged");
		}
		protected override void OnCanvasHierarchyChanged ()
		{
			base.OnCanvasHierarchyChanged ();
			//Debug.LogError ("OnCanvasHierarchyChanged");
		}
		protected override void OnCanvasGroupChanged ()
		{
			base.OnCanvasGroupChanged ();
			//Debug.LogError ("OnCanvasGroupChanged");
		}
		protected override void OnRectTransformDimensionsChange ()
		{
			base.OnRectTransformDimensionsChange ();
			//Debug.LogError ("OnRectTransformDimensionsChange");
		}

	}
}
