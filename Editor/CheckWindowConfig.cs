﻿/************************************************************************************
 * @author   wangjian
 * 窗口配置类
************************************************************************************/

using UnityEngine;
using UnityEditor;
using System.Collections;
using Client.ApplicationUI;
using Client.Utils;
namespace Client.ApplicationUI.Editor{
	public class CheckResourceConfig :ScriptableObject {
		

		[MenuItem("开发库/ApplicationUI/ChecResourceConfig")]
		public static void OnReadConfig(){

			string fileName = Application.streamingAssetsPath + "/Config/" + "/WindowConfig.xml";
			WindowConfig temp = WindowConfig.Read (fileName);
			foreach (WindowNode value in temp.Nodes) {
				Debug.LogErrorFormat ("type:{0} res:{1} load_mode：{2} proriy:{3} layer:{4}",value.type,value.res,value.loadMode,value.prioriy,value.layer);
			}
		}

		[MenuItem("开发库/ApplicationUI/Save_ExampleResourceConfig")]
		public static void OnSaveExConfig(){
			WindowConfig config = new WindowConfig(); 
			config.Nodes.Add (new WindowNode ("MainRootUI", "Ex_MainRootUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.HIGHEST, SHOW_LAYER.Layer2,"所有UI的根节点"));
			config.Nodes.Add (new WindowNode ("MainUI", "Ex_MainUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面"));
			config.Nodes.Add (new WindowNode ("TopMenu", "Ex_TopMenu", UILOAD_MODE.PART, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面的顶部菜单UI"));

			config.Nodes.Add (new WindowNode ("MessageBox1Button", "Ex_MessageBox1Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"一个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBox2Button", "Ex_MessageBox2Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"两个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBoxCheckBox", "Ex_MessageBoxCheckBox", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"CheckBox"));

			config.Nodes.Add (new WindowNode ("Countdown", "Ex_Countdown", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"倒计时弹出框"));



			string fileName = Application.streamingAssetsPath + "/Config/"+Config.GetWindowConfigPrefix(UIResType.Example)+Config.windowConfigFileName;
			WindowConfig.Save (config,fileName);
		}

		[MenuItem("开发库/ApplicationUI/TestSave_PC[慎点]")]
		public static void OnSavePCConfig(){
			WindowConfig config = new WindowConfig(); 
			config.Nodes.Add (new WindowNode ("MainRootUI", "PC_MainRootUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.HIGHEST, SHOW_LAYER.Layer2,"所有UI的根节点"));
			config.Nodes.Add (new WindowNode ("MainUI", "PC_MainUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面"));
			config.Nodes.Add (new WindowNode ("TopMenu", "PC_TopMenu", UILOAD_MODE.PART, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面的顶部菜单UI"));

			config.Nodes.Add (new WindowNode ("MessageBox1Button", "PC_MessageBox1Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"一个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBox2Button", "PC_MessageBox2Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"两个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBoxCheckBox", "PC_MessageBoxCheckBox", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"CheckBox"));

			config.Nodes.Add (new WindowNode ("Countdown", "PC_Countdown", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"倒计时弹出框"));



			string fileName = Application.streamingAssetsPath + "/Config/"+Config.GetWindowConfigPrefix(UIResType.PC)+Config.windowConfigFileName;
			WindowConfig.Save (config,fileName);
		}

		[MenuItem("开发库/ApplicationUI/Test_Save_iPhone[慎点]")]
		public static void OnSaveiPhoneConfig(){
			WindowConfig config = new WindowConfig(); 
			config.Nodes.Add (new WindowNode ("MainRootUI", "IP_MainRootUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.HIGHEST, SHOW_LAYER.Layer2,"所有UI的根节点"));
			config.Nodes.Add (new WindowNode ("MainUI", "IP_MainUI", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面"));
			config.Nodes.Add (new WindowNode ("TopMenu", "IP_TopMenu", UILOAD_MODE.PART, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"主界面的顶部菜单UI"));

			config.Nodes.Add (new WindowNode ("MessageBox1Button", "IP_MessageBox1Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"一个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBox2Button", "IP_MessageBox2Button", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"两个按钮的弹出框"));
			config.Nodes.Add (new WindowNode ("MessageBoxCheckBox", "IP_MessageBoxCheckBox", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"CheckBox"));

			config.Nodes.Add (new WindowNode ("Countdown", "IP_Countdown", UILOAD_MODE.WINDOW, SHOW_PRIORIY.MIDDLE, SHOW_LAYER.Layer3,"倒计时弹出框"));



			string fileName = Application.streamingAssetsPath + "/Config/"+Config.GetWindowConfigPrefix(UIResType.iOS)+Config.windowConfigFileName;
			WindowConfig.Save (config,fileName);
		}

	}


}
