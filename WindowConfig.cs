﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.IO;
using Client.Utils;


namespace Client.ApplicationUI{
	[Serializable()]
	public class WindowConfig  {

		public List<WindowNode> Nodes = new List<WindowNode>();


		public static WindowConfig Read(string fileName){
			return FileHelper.XMLToObject<WindowConfig> (fileName);
		}

		public static void Save(WindowConfig data,string fileName ){
			FileHelper.ObjectToXML<WindowConfig> (data, fileName);

		}


	}
	[Serializable()]
	public class WindowNode{
		public string note;			//注释
		public string type;			//窗口类型
		public string res;			//资源key
		public int loadMode;		//加载模式
		public int prioriy; 		//优先级
		public int layer;

		public WindowNode(){}
		public WindowNode(string _type,string _res,UILOAD_MODE _loadMode,SHOW_PRIORIY _prioriy,SHOW_LAYER _layer,string _note=""){
			type = _type;
			res = _res;
			loadMode = (int)_loadMode;
			prioriy = (int)_prioriy;
			layer = (int)_layer;
			note = _note;
		}
	}



}
