﻿using UnityEngine;
using System.Collections;
using Client.Utils;

namespace Client.ApplicationUI{

	//使用哪个平台UI资源
	public enum UIResType{
		Example,			//一种特殊PC，用于演示
		PC,					//windows pc
		iPad,				
		iOS,
		AndroidPad,
		AndroidPhone

	}

	public class Config{

		public static UIResType uiResType = UIResType.Example;
		public const string windowConfigFileName = "WindowConfig.xml";

		private static string _windowConfigPath;
		public static string WindowConfigPath {
			get{
				if (string.IsNullOrEmpty (_windowConfigPath)) {
					_windowConfigPath = FileHelper.FormatPath (CommonVariable.PersistentsDataPath + CommonVariable.ConfigDirectory);
				}
				return _windowConfigPath;
			}

		}

		public static string GetWindowConfigPrefix(){
			return GetWindowConfigPrefix (uiResType);
		}

		public static string GetWindowConfigPrefix(UIResType uiResType){
			string str = "";
			switch (uiResType) {
			case UIResType.PC:
				str = "PC_";
				break;
			case UIResType.Example:
				str =  "Ex_";
				break;
			case UIResType.iOS:
				str =  "iOS_";
				break;
			case UIResType.iPad:
				str =  "iPad_";
				break;
			case UIResType.AndroidPad:
				str =  "AndPad_";
				break;

			case UIResType.AndroidPhone:
				str =  "AndPhone_";
				break;
			
				
			}
			return str;
		}





	}
}
