﻿using UnityEngine;
using System.Collections;
using Client.ApplicationUI;

public class IWindow : MonoBehaviour {


	private string _windowType = "";
	public string windowType{
		set{ 
			_windowType = value;
		}
	}

	protected virtual void OnClickCloseWindow(){
		ApplicationUI.Instance.closeWindowObject (_windowType, SHOW_EFFECT.NONE, true);
	}
}
