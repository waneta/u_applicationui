﻿using UnityEngine;
using System.Collections;
namespace Client.ApplicationUI{

	public enum SHOW_EFFECT
	{
		NONE,					//无效果

		LEFTMOVESHOW,		
		RIGHTMOVESHOW,
		MOSTLEFTMOVESHOW,
		MOSTRIGHTMOVESHOW,

		LEFTMOVEHIDE,		
		RIGHTMOVEHIDE,
		MOSTLEFTMOVEHIDE,
		MOSTRIGHTMOVEHIDE,

		FADE,					//渐隐
		ZOOM,					//缩放
		COUNT,
	}

	//显示的优先级
	public enum SHOW_PRIORIY
	{
		NONE = 0 ,
		BOTTOM = 1,                 //最底层
		MIDDLE = 2,                 //中层
		HIGHEST = 3,                //最高层
	}
	//同一优先级的不同显示层，Layer1最顶层，Layer18最底层
	public enum SHOW_LAYER {
		NONE = 0 ,
		Layer1,
		Layer2,
		Layer3,
		Layer4,
		Layer5,
		Layer6,
		Layer7,
		Layer8,
		Layer9,
		Layer10,
		Layer12,
		Layer13,
		Layer14,
		Layer15,
		Layer16,
		Layer17,
		Layer18,
	}
	public enum UILOAD_MODE
	{
		NONE = 0,
		WINDOW = 1,                 //窗口
		PART = 2,                   //组成UI
	}

	public enum UI_STATE{
		SHOWING,		        //正在移动至正中显示的过度
		SHOW,			        //UI在屏幕正中显示的状态

		LEFTHIDE,		        //向屏幕左边移动至全部隐藏之后的UI 显示状态
		RIGHTHIDE,		        //向屏幕右边移动至全部隐藏之后的UI 显示状态
		LEFTMOSTHIDE,	        //向屏幕左边移动至大部分隐藏之后的UI 显示状态
		RIGHMOSTFHIDE,	        //向屏幕右边移动至大部分隐藏之后的UI 显示状态
		HIDE,			        //other 隐藏
		HIDING,			        //正在移动至隐藏的过度
	}

}
