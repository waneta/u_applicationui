﻿/************************************************************************************
 * @author   wangjian
 * Application UI manager
************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using DG.Tweening;
using Client.Utils;
using UI.Common;

namespace Client.ApplicationUI{

    public class WindowObject
    {
        public UI_STATE state = UI_STATE.HIDE;
        public GameObject windowObject;
		public string type;
        public UILOAD_MODE loadMode;
        public string res;
        public int destoryTimeID = -1;
        public SHOW_EFFECT effect;
        public bool isDestroy = false;
        public SHOW_LAYER layer = SHOW_LAYER.NONE;
        public SHOW_PRIORIY prioriy = SHOW_PRIORIY.NONE;
		public WindowObject(string t, UILOAD_MODE m, string r, SHOW_PRIORIY p, SHOW_LAYER l )
        {
            windowObject = null;
            type = t;
            loadMode = m;
            res = r;
            prioriy = p;
            layer = l;
        }
    }
    public class ApplicationUI :Singleton<ApplicationUI> {


		private Dictionary<string,WindowObject> objects = new Dictionary<string, WindowObject>();
		private List<GameObject> showObjects = new List<GameObject>();
		//private List<GameObject> hideObjects = new List<GameObject>();

    	public string getState(){
			string str = "";
			foreach(KeyValuePair<string,WindowObject> pair in objects){
				str += string.Format( "T:{0}______E:{1}______S:{2}",pair.Value.type,pair.Value.effect,pair.Value.state)+"\n"; 
			}
			return str;
		}

		public ApplicationUI ()
		{
			WindowConfig config = WindowConfig.Read (Application.streamingAssetsPath + "/Config/"+Config.GetWindowConfigPrefix()+Config.windowConfigFileName);
			foreach (WindowNode node in config.Nodes) {
				//Debug.LogErrorFormat ("***type:{0} res:{1} load_mode：{2} proriy:{3} layer:{4}",node.type,node.res,node.loadMode,node.prioriy,node.layer);
				AddWindowObject(node.type, (UILOAD_MODE)node.loadMode, node.res, (SHOW_PRIORIY)node.prioriy, (SHOW_LAYER)node.layer);
			}
        }

		private void AddWindowObject(string type,UILOAD_MODE mode,string res,SHOW_PRIORIY prioriy, SHOW_LAYER layer)
		{
			objects.Add(type,new WindowObject(type,mode,ResourceDefine.GetResourceName(res), prioriy, layer));
		}
		#region UI Event

		#endregion

		Canvas _mainCanvas;

        public Canvas MainCanvas
        {
            get { return _mainCanvas; }
        }


		public void init(UIResType _resType){
			Config.uiResType = _resType;
			if(mainUIObject==null){
				LogManager.Instance.info("UI init...");
				mainUIObject = ResourceManager.Instance.InstantiateGameObjectFromResource(ResourceDefine.GetResourceName("PC_MainRootUI"));
				mainUIObject.name = "MainRootUI";
                Common.AddComponent<DontDestroyer>(mainUIObject);
				mainUIObject.transform.position = new Vector3(0,0,0);
				_mainCanvas = mainUIObject.GetComponent<Canvas>();
				//CanvasScaler _mainCanvaseScaler  = mainUIObject.GetComponent<CanvasScaler>();

				//此处设置画布分辨率
				//*_*LogManager.Instance.error("Screen width :{0}  height:{1} ",ClientConfig.width,ClientConfig.height);
				//if(_mainCanvaseScaler!=null){
				//	_mainCanvaseScaler.referenceResolution = new Vector2(ClientConfig.width,ClientConfig.height);
				//}
			}else{
				LogManager.Instance.info("MainUIRoot is not null");
			}
		}
		//所有Ui的根节点
        private GameObject _mainUIObject;
		public GameObject mainUIObject{
			get{
				if(_mainUIObject==null){
					_mainUIObject = GameObject.Find("MainRootUI");
				}
				return _mainUIObject;
			}
			set{
				_mainUIObject = value;
			}
		}
	


		void openWindow(ref GameObject obj,string resName)
		{
			if (obj != null)
			{
				return;
			}
			obj = ResourceManager.Instance.InstantiateGameObjectFromResource(resName);
			if (obj == null)
			{
				LogManager.Instance.error("ui resource is null  {0}",resName);
				return;
			}
		}

		void closeWindow (ref GameObject obj)
		{
			if (obj == null)
				return;
			Object.Destroy (obj);
			obj = null;
		}


		public WindowObject getWindowObject(string type)
		{
			if (objects.ContainsKey(type))
			{
				 WindowObject obj = objects[type];
				return obj;
			}
			return null;			
		}

		public T getWindowObject<T>(string type) where T : Component
		{
			GameObject obj = getWindowObject(type).windowObject;
			if (obj != null)
				return obj.GetComponent<T>();
			return null;			
		}
       /// <summary>
       /// isShadow 是否需要阴影
       /// </summary>
       /// <param name="type"></param>
       /// <param name="effect"></param>
       /// <param name="isShadow"></param>
       /// <returns></returns>
		public GameObject openWindowObject(string type, SHOW_EFFECT effect, bool isShadow = false, bool isNeenCloseShadow = true)
		{
            LogManager.Instance.error("openWindowObject "+ type);
			if (objects.ContainsKey (type)) {
				WindowObject obj = objects [type];
				obj.effect = effect;

				if (obj.windowObject != null) {
					if (obj.state != UI_STATE.SHOWING && obj.state != UI_STATE.SHOW) {
						if (!obj.windowObject.activeInHierarchy)
							obj.windowObject.SetActive (true);

						Common.StartCoroutine (showWindowObjectEffect (obj, type, isShadow, isNeenCloseShadow));

					}
						
					return obj.windowObject;
				}

				openWindow (ref obj.windowObject, obj.res);

				if (obj.windowObject != null) {
					IWindow w = obj.windowObject.GetComponent<IWindow> ();
					if (w != null) {
						w.windowType = type;
					}
				}
				showObjects.Add (obj.windowObject);

				obj.state = UI_STATE.SHOWING;
				Common.StartCoroutine (showWindowObjectEffect (obj, type, isShadow, isNeenCloseShadow));
				return obj.windowObject;
			} else {
				LogManager.Instance.error("openWindowObject {0}  objects not Contain", type);
			}
			return null;
		}

        //设置窗口显示层次
		private void SetWindowLayer(string type)
        {
            WindowObject obj = getWindowObject(type);
            List<WindowObject> prioriy_histest = new List<WindowObject>();
            List<WindowObject> prioriy_bottom = new List<WindowObject>();
            List<WindowObject> prioriy_middle = new List<WindowObject>();

            if (obj != null && obj.windowObject!=null)
            {
                if (obj.prioriy == SHOW_PRIORIY.NONE || obj.layer == SHOW_LAYER.NONE) return;

                foreach (KeyValuePair<string, WindowObject> pair in objects)
                {
                    if (pair.Value.type == obj.type) continue;
                    if(pair.Value.state == UI_STATE.SHOW || pair.Value.state == UI_STATE.SHOWING)
                    {
                        if(pair.Value.prioriy == SHOW_PRIORIY.BOTTOM)
                        {
                            prioriy_histest.Add(pair.Value);
                        }else if (pair.Value.prioriy == SHOW_PRIORIY.MIDDLE)
                        {
                            prioriy_middle.Add(pair.Value);
                        }else if (pair.Value.prioriy == SHOW_PRIORIY.HIGHEST)
                        {
                            prioriy_histest.Add(pair.Value);
                        }
                    }
                }


                int _siblingIndex = 0;

                if(obj.prioriy == SHOW_PRIORIY.BOTTOM)
                {
                    for (int i = 0; i < prioriy_bottom.Count; i++)
                    {
                        if (obj.layer <= prioriy_bottom[i].layer)
                        {
                            _siblingIndex++;
                        }
                    }
                    
                }else if (obj.prioriy == SHOW_PRIORIY.MIDDLE)
                {
                    _siblingIndex += prioriy_bottom.Count;

                    for (int i = 0; i < prioriy_middle.Count; i++)
                    {
                        if (obj.layer <= prioriy_middle[i].layer)
                        {
                            _siblingIndex++;
                        }
                    }
                }
                else if (obj.prioriy == SHOW_PRIORIY.HIGHEST)
                {
                    _siblingIndex += prioriy_bottom.Count;
                    _siblingIndex += prioriy_middle.Count;

                    for (int i = 0; i < prioriy_histest.Count; i++)
                    {
                        if (obj.layer <= prioriy_histest[i].layer)
                        {
                            _siblingIndex++;
                        }
                    }
                }
                obj.windowObject.GetComponent<RectTransform>().SetSiblingIndex(_siblingIndex);
            }

        }

        private IEnumerator showWindowObjectEffect(WindowObject obj,string type, bool isShadow = false, bool isNeenCloseShadow = true)
		{
			if (obj.windowObject == null)
			{
				LogManager.Instance.error("showWindowObjectEffect obj.windowObject is null   {0}",obj.type);
				yield break;
			}
            obj.windowObject.name = string.Format("{0}_{1}_{2}", type.ToString(), obj.prioriy, obj.layer);
            
            RectTransform rectTransform = obj.windowObject.GetComponent<RectTransform>();
			if (_mainCanvas != null) {
				obj.windowObject.transform.SetParent (_mainCanvas.transform);
				obj.windowObject.transform.localPosition = Vector3.zero;
				obj.windowObject.transform.localScale = Vector3.one;
				obj.windowObject.transform.localEulerAngles = Vector3.zero;
			}
			//if(obj.type == "PC_MainUI")//主窗口全部填充
			if(obj.type.Contains("MainUI"))
            {
                rectTransform.anchorMin = new Vector2(0f, 0f);
                rectTransform.anchorMax = new Vector2(1f, 1);
                rectTransform.offsetMax = new Vector2(0, 0);
                rectTransform.offsetMin = new Vector2(0, 0);
                rectTransform.pivot = new Vector2(0.5f, 0.5f);
            }


            SetWindowLayer(type);

            GameObject ShadowG=null;
            if (isShadow) //如果需要显示背景遮罩
            {
				ShadowG = ResourceManager.Instance.InstantiateGameObjectFromResource(ResourceDefine.GetResourceName("WindowShadow"));
                if (_mainCanvas!=null)
                   ShadowG.transform.SetParent(_mainCanvas.transform);
				
                ShadowG.GetComponent<RectTransform>().anchorMax=Vector2.one;
                ShadowG.GetComponent<RectTransform>().anchorMin=Vector2.zero;
                ShadowG.GetComponent<RectTransform>().pivot=new Vector2(0.5f,0.5f);
                ShadowG.GetComponent<RectTransform>().anchoredPosition3D=Vector3.zero;
                ShadowG.GetComponent<RectTransform>().sizeDelta=Vector2.zero;
                ShadowG.GetComponent<RectTransform>().localScale=Vector3.one;
                ShadowG.name = "Shadow";
                ShadowG.transform.SetParent(obj.windowObject.transform);
                ShadowG.transform.SetSiblingIndex(0);
                ShadowG.gameObject.GetComponent<Button>().enabled = false;
				Debug.LogError("==================0");
                DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
                ShadowG.gameObject.GetComponent<Image>().DOFade(0.8f, 1.0f).OnComplete(delegate() {
                    if (isNeenCloseShadow)
                    {
						Debug.LogError("==================1");
                        ShadowG.gameObject.GetComponent<Button>().enabled = true;
						ShadowG.gameObject.GetComponent<WindowShadow>().windowType = type;
                    }
                });

                if (!isNeenCloseShadow)
                {
					Debug.LogError("==================2");
                    ShadowG.gameObject.GetComponent<Button>().enabled = false;
                }
            }

			if (obj.effect == SHOW_EFFECT.FADE){



            }else if (obj.effect == SHOW_EFFECT.ZOOM)
			{
                Tween tweener = rectTransform.DOScale(Vector3.one, 0.3f);
                tweener.SetEase(Ease.Linear);
                tweener.OnComplete(delegate()
                {
                    Common.StartCoroutine(OnShowPositionFinished(obj.type));
                    
                });
			}else
            {
                obj.state = UI_STATE.SHOW;
                rectTransform.localScale = Vector3.one;
            }
		}

		IEnumerator OnShowPositionFinished(object o)
		{
			string type = (string)o;
			if (objects.ContainsKey(type))
			{
				WindowObject obj = objects[type];
				obj.state = UI_STATE.SHOW;
			}
			yield break;
		}
        /// <summary>
        /// isShadow是否显示阴影
		/// isNeenCloseShadow 点击阴影部分是否关闭窗口
		public T openWindowObject<T>(string type,SHOW_EFFECT effect,bool isShadow=false,bool isNeenCloseShadow=true) where T : Component
		{
			GameObject obj = openWindowObject(type,effect,isShadow,isNeenCloseShadow);
			if (obj != null)
				return obj.GetComponent<T>();
			return null;
		}

		public void closeWindowObject(string type,SHOW_EFFECT effect,bool isDestroy)
		{
            LogManager.Instance.error("closeWindowObject " + type.ToString());
            if (objects.ContainsKey(type))
			{
				WindowObject obj = objects[type];
				if(!showObjects.Contains(obj.windowObject)) return;
				if (obj.state != UI_STATE.HIDE  && obj.state != UI_STATE.HIDING || (obj.state == UI_STATE.HIDE && isDestroy) )
				{
					obj.state = UI_STATE.HIDING;
					obj.isDestroy = isDestroy;
					obj.effect = effect;
                    if(obj!=null)
					   hideWindowObjectEffect(obj);
				}
                if(isDestroy){
                  
                }
			}
		}

		public override void initialize(){
			//LogManager.Instance.info("override App initialize {0}",this.ToString());

		}

		private void hideWindowObjectEffect(WindowObject obj)
		{
			if (obj.windowObject == null)
			{
				LogManager.Instance.error("hideWindowObjectEffect obj.windowObject is null   {0}",obj.type);
				return;
			}

            Transform shadowG=obj.windowObject.transform.FindChild("Shadow");
            if(shadowG) {
                 shadowG.gameObject.SetActive(false);
                 GameObject.Destroy(shadowG.gameObject);
            }

			RectTransform rectTransform = obj.windowObject.GetComponent<RectTransform>();
			if (obj.effect == SHOW_EFFECT.LEFTMOVEHIDE || obj.effect == SHOW_EFFECT.RIGHTMOVEHIDE ||obj.effect == SHOW_EFFECT.MOSTLEFTMOVEHIDE ||obj.effect == SHOW_EFFECT.MOSTRIGHTMOVEHIDE)
			{

//				float XPosition = rectTransform.localPosition.x;
				float YPositon = rectTransform.localPosition.y;
				float ZPositoin = rectTransform.localPosition.z;
				float moveSpace = 640;
				if(SHOW_EFFECT.RIGHTMOVEHIDE == obj.effect){
					moveSpace = 900;
				}else if(SHOW_EFFECT.MOSTLEFTMOVEHIDE == obj.effect){
					moveSpace = -600;
				}else if(SHOW_EFFECT.MOSTRIGHTMOVEHIDE == obj.effect){
					moveSpace = 600;
				}
				else if(SHOW_EFFECT.LEFTMOVEHIDE == obj.effect){
					moveSpace = -640;
				}

				Tween tweener = rectTransform.DOLocalMove(new Vector3(moveSpace, YPositon, ZPositoin),0.35f,false);
				tweener.SetEase(Ease.Linear);
				obj.destoryTimeID = TimeCallManager.GetInstance().addTimeCall(0.35f,new TimeCall.OnTimeCallOver(OnHidePositionFinished),obj.type);

			}
			else if (obj.effect == SHOW_EFFECT.FADE)
			{

			}
			else if (obj.effect == SHOW_EFFECT.ZOOM)
			{
                Tween tweener = rectTransform.DOScale(Vector3.one*0.5f, 0.2f);
                tweener.SetEase(Ease.Linear);
                tweener.OnComplete(delegate()
                {
                   
                    Common.StartCoroutine(OnHidePositionFinished(obj.type));
                    if (shadowG) GameObject.Destroy(shadowG.gameObject);
                });
			}
			else if(obj.effect == SHOW_EFFECT.NONE)
			{
				Common.StartCoroutine(OnHidePositionFinished(obj.type));
			}
		}

		IEnumerator OnHidePositionFinished(object o)
		{
			string type = (string)o;
			if (objects.ContainsKey(type))
			{
				WindowObject obj = objects[type];
				if(obj.isDestroy ){
					obj.state = UI_STATE.HIDE;
					
					obj.destoryTimeID = -1;
					obj.windowObject.SendMessage("onHidePositionFinished",SendMessageOptions.DontRequireReceiver);
					showObjects.Remove(obj.windowObject);
					closeWindow(ref obj.windowObject);
					//ResourceManager.Instance.dealloc();
				}else{ //不销毁
					obj.state = UI_STATE.HIDE;
                    obj.windowObject.SetActive(false);
				}
			}
			//*_*statestring = getState();
			yield break;
		}

        public T LoadUI<T>(string type,RectTransform rect) where T :Component
        {
            if (objects.ContainsKey(type))
            {
                WindowObject obj = objects[type];
                obj.effect = SHOW_EFFECT.NONE;

                if (obj.windowObject == null)
                {
                    openWindow(ref obj.windowObject, obj.res);

                    showObjects.Add(obj.windowObject);

                    obj.windowObject.transform.SetParent(rect);
                    RectTransform rectTransform = obj.windowObject.GetComponent<RectTransform>();
                    //rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);

                    if(type == "IP_TOPSWITCHMMENUUI")

                    {
                        rectTransform.pivot = new Vector2(1f, 0.5f);
                        rectTransform.anchorMin = new Vector2(0f, 0f);
                        rectTransform.anchorMax = new Vector2(1f, 1);
                        rectTransform.offsetMax = new Vector2(0, 0);


                        rectTransform.offsetMin = new Vector2(0, 0);
                        //rectTransform.localPosition = Vector3.zero;

                        rectTransform.localScale = Vector3.one;

                    }
                    else
                    {
                        rectTransform.anchorMin = new Vector2(0f, 0f);
                        rectTransform.offsetMax = new Vector2(0, 0);

                        rectTransform.anchorMax = new Vector2(1f, 1);
                        rectTransform.offsetMin = new Vector2(0, 0);

                        rectTransform.pivot = new Vector2(0.5f, 0.5f);
                        rectTransform.localPosition = Vector3.zero;
                        rectTransform.localScale = Vector3.one;
                    }



                    obj.state = UI_STATE.SHOW;
                }

                if (obj.windowObject != null)
                {
                    return obj.windowObject.GetComponent<T>();
                }

            }
            return null;
        }

        public Coroutine LoadUI(string type, RectTransform rect, CoroutineParam param)
        {
            return Common.StartCoroutine(LoadUI_impl(type,rect, param));
        }

        IEnumerator LoadUI_impl(string type, RectTransform rect, CoroutineParam param)
        {
            if (objects.ContainsKey(type))
            {
                WindowObject obj = objects[type];
                obj.effect = SHOW_EFFECT.NONE;

                if (obj.windowObject == null)
                {
                    if (obj.windowObject != null)
                    {
                        yield break;
                    }
                    CoroutineParam objParam = new CoroutineParam();
                    yield return ResourceManager.Instance.InstantiateGameObjectFromResource(obj.res, objParam);
                    obj.windowObject = objParam.param as GameObject;
                    if (obj.windowObject == null)
                    {
                        LogManager.Instance.error("ui resource is null  {0}", obj.res);
                        yield break;
                    }

                    obj.windowObject.SendMessage("onInitializeGameObject", SendMessageOptions.DontRequireReceiver);
                    showObjects.Add(obj.windowObject);

                    obj.windowObject.transform.SetParent(rect);
                    RectTransform rectTransform = obj.windowObject.GetComponent<RectTransform>();

                    rectTransform.anchorMin = new Vector2(0f, 0f);
                    rectTransform.offsetMax = new Vector2(0, 0);

                    rectTransform.anchorMax = new Vector2(1f, 1);
                    rectTransform.offsetMin = new Vector2(0, 0);

                    rectTransform.pivot = new Vector2(0.5f, 0.5f);

                    rectTransform.localPosition = Vector3.zero;
                    rectTransform.localScale = Vector3.one;
                    obj.state = UI_STATE.SHOW;
                }

                param.param = obj;

            }

            yield break;
        }


    }
}