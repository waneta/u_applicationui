﻿using UnityEngine;
using System.Collections;
using Client.ApplicationUI;

namespace UI.Common{
	public class WindowShadow : MonoBehaviour {

		public string _windowType ="";
		public string windowType{
			set{ 
				_windowType = value;
			}
		}
		public void OnClickWindowShadow(){
			if (string.IsNullOrEmpty (_windowType))
				return;
			ApplicationUI.Instance.closeWindowObject (_windowType, SHOW_EFFECT.NONE, true);
		}

	}
}
