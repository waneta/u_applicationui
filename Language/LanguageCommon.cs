﻿/************************************************************************************
 * @author   wangjian
 * 多语言
************************************************************************************/

namespace Client.Language
{
    public class LanguageCommon
    {

        public static string GetValue(string key,string defaultValue= "Error Language!")
        {
            return LocalLanguage.instance.GetValue(key,defaultValue);
        }
        public static string getStringMessage(int key)
        {
            return LocalLanguage.instance.getStringMessage(key);
        }

    }
}