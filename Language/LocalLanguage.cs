﻿/************************************************************************************
 * @author   wangjian
 * 多语言
************************************************************************************/
using System;
using UnityEngine;
using System.Collections.Generic;
//*_*using Client.TableConversion;
//using mySwing;
using Client.Utils;

namespace Client.Language
{
	public enum EnumLanguage {
		English,
		Japanese,
		Chinese,
	}
	public class Language { 
		private string m_sName="";				  //// 字段名称 
		public string Name { get { return m_sName; } } 
		private string m_sValue_ChineseSimplified="";				  //// 字段值 
		public string Value_ChineseSimplified { get { return m_sValue_ChineseSimplified; } } 
		private string m_sValue_English="";				  //// 字段值英文 
		public string Value_English { get { return m_sValue_English; } } 
		private string m_sValue_Japanese="";				  //// 字段值日文 
		public string Value_Japanese { get { return m_sValue_Japanese; } } 
	}
	public class StringMessage { 
		private Int32 m_nStringID=0;				  //// 字符串ID 
		public Int32 ID { get { return m_nStringID; } } 
		public Int32 StringID { get { return m_nStringID; } } 
		private string m_sMessage_ChineseSimplified="";				  //// 字符串 
		public string Message_ChineseSimplified { get { return m_sMessage_ChineseSimplified; } } 
		private string m_sMessage_English="";				  //// 字段值英文 
		public string Message_English { get { return m_sMessage_English; } } 
	}
    public class LocalLanguage : MonoBehaviour
    {
        public static LocalLanguage mInst;
        //public UnityEngine.Object fontObj;
        static public LocalLanguage instance
        {
            get
            {
                if (mInst == null)
                {
                    mInst = UnityEngine.Object.FindObjectOfType(typeof(LocalLanguage)) as LocalLanguage;

                    if (mInst == null)
                    {
                        GameObject go = new GameObject("_LocalLanguage");
                        DontDestroyOnLoad(go);
                        mInst = go.AddComponent<LocalLanguage>();
                        //mInst.fontObj = Resources.Load("IphoneUI/STXINWEI");
                    }
                    mInst.initialize();
                }
                return mInst;
            }
        }
		private Dictionary<string, Language> _list = new Dictionary<string, Language>();
		private Dictionary<Int32, StringMessage> _stringlist = new Dictionary<Int32, StringMessage>();

        private bool _bInitializeLanguage = false;
        private bool _bInitialize = false;
        public const string M_EVT_LOCAL_LANGUAGE_CHANGED = "m_evt_localization_language_changed";
        private EnumLanguage mLanguage = EnumLanguage.English;
        public EnumLanguage CurrentLanguage
        {
            get { return mLanguage; }
            set
            {
                if (mLanguage != value)
                {
                    mLanguage = value;
                    Messenger.Broadcast(M_EVT_LOCAL_LANGUAGE_CHANGED, MessengerMode.DONT_REQUIRE_LISTENER);
                }
            }
        }

        private void initialize()
        {
            if (_bInitialize == false)
            {
                _list.Clear();
                /*foreach (KeyValuePair<Int32, MT_Data_Language> pair in TableManager.Instance.TableLanguage.Datas())
                {
                    if (_list.ContainsKey(pair.Value.Name))
                    {
                        Debug.LogError("Language key is exist: " + pair.Value.Name);
                        continue;
                    }
                    if (!string.IsNullOrEmpty(pair.Value.Name))
                        _list.Add(pair.Value.Name, pair.Value);
                }*/
                _bInitialize = true;
                initializeLanguage();
            }
        }

        public void refreshLanguage()
        {
            _bInitializeLanguage = false;
            initializeLanguage();
        }
        private void initializeLanguage()
        {
            if (_bInitializeLanguage == false)
            {
                //*_*CurrentLanguage = ClientConfig.language;
                _bInitializeLanguage = true;
            }
        }
        public string GetValue(string key,string defaultVal= "Error Language!")
        {
            if (!_list.ContainsKey(key)) return defaultVal;
            Language dsm = _list[key];
            if (dsm == null)
                return defaultVal;

            if (CurrentLanguage == EnumLanguage.English)
                return dsm.Value_English;
            else if (CurrentLanguage == EnumLanguage.Japanese)
                return dsm.Value_Japanese;
            else if (CurrentLanguage == EnumLanguage.Chinese)
            {
                return dsm.Value_ChineseSimplified;
            }
            return defaultVal;
        }
        public string getStringMessage(int key)
        {

			StringMessage dsm = _stringlist[key];
            
            if (dsm == null)
                return "Error Language!";

            if (CurrentLanguage == EnumLanguage.English)
                return dsm.Message_English;
            else if (CurrentLanguage == EnumLanguage.Chinese)
            {
                return dsm.Message_ChineseSimplified;
            }
            return "Error Language!";
        }

    }
}