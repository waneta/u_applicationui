﻿/************************************************************************************
 * @author   wangjian
 * 多语言
************************************************************************************/
using UnityEngine;
using UnityEngine.UI;
namespace Client.Language
{

    public class UILocalize : MonoBehaviour
    {

        public string key;

        //bool mStarted = false;
        //void OnEnable() { if (mStarted && LocalLanguage.instance != null) Localize(); }


        void Start()
        {
            //mStarted = true;
            if (LocalLanguage.instance != null) Localize();
        }


        public void Localize()
        {
            LocalLanguage loc = LocalLanguage.instance;
            Text lbl = GetComponent<Text>();
            Image sp = GetComponent<Image>();

            if (string.IsNullOrEmpty(key) && lbl != null) key = lbl.text;
            string val = "";

            if (lbl != null)
            {
                val = string.IsNullOrEmpty(key) ? loc.GetValue(lbl.name) : loc.GetValue(key);
                lbl.text = val;
            }
            else if (sp != null)
            {
                //*_*/sp.spriteName = val;
                //*_*/sp.MakePixelPerfect();
            }
        }
    }
}